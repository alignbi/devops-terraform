variable "region" {
  default = "us-west-2"
}

variable "availability_zone" {
  default = "us-west-2a"
}

variable "vpc" {
  description = "VPC -> us-west-2 (oregon) 10.84.0.0/16"
  default = "vpc-7e7bb61a"
}

variable "private_subnet" {
  description = "private subnet 1 (us-west-2a)  10.84.100.0/24"
  default = {
    id = "subnet-75e43011"
    cidr = "10.84.100.0/24"
  }
}

variable "public_subnet" {
  description = "public subnet 1 (us-west-2a) 10.84.0.0/24"
  default = {
    id = "subnet-58f92d3c"
    cidr = "10.84.0.0/24"
  }
}

variable "instance_ips" {
  default = {
    "0" = "10.84.0.65"
    "1" = "10.84.0.66"
    "2" = "10.84.0.67"
    "3" = "10.84.0.68"
    "4" = "10.84.0.69"
  }
}

# These hostnames will be added to /etc/hosts, then can be provisioned with ansible
variable "instance_hostnames" {
  default = {
    "0" = "an-master01"
    "1" = "an-slave01"
    "2" = "an-slave02"
    "3" = "an-slave03"
    "4" = "an-slave04"
  }
}

variable "ssh_key_file" {
  description = "public key for launching of instances"
  default = "/root/.ssh/aws_deploy_rsa.pub"
}

variable "amazon_linux_amis" {
  default = {
    us-west-1 = "ami-31490d51"
    us-west-2 = "ami-7172b611"
    us-east-1 = "ami-6869aa05"
    eu-west-1 = "ami-f9dd458a"
  }
}

variable "redhat_7_amis" {
  default = {
    us-west-1 = "ami-d1315fb1"
    us-west-2 = "ami-775e4f16"
    us-east-1 = "ami-2051294a"
    eu-west-1 = "ami-8b8c57f8"
  }
}
