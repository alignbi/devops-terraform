# Make sure following aws user access/secret keys are in ~/.aws/credentials
provider "aws" {
  profile    = "terraform"
  region     = "${var.region}"
}

# Placement group for cluster
resource "aws_placement_group" "analytics-pg" {
  name       = "analytics-pg"
  strategy   = "cluster"
}

# Cluster security group
resource "aws_security_group" "cluster" {
  name        = "analytics-cluster-sg"
  description = "sg for analytics cluster"
  vpc_id      = "${var.vpc}"

  # SSH access from home
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["208.53.40.18/32",]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "aws_deploy" {
  key_name = "aws_deploy"
  public_key = "${file(var.ssh_key_file)}"
}

resource "aws_instance" "analytics-cluster" {
  count = 5
  ami           = "${lookup(var.redhat_7_amis, var.region)}"
  instance_type = "t2.micro"
  availability_zone = "${var.availability_zone}"
  # t2.micro without placement group for testing purposes
  #placement_group = "${aws_placement_group.analytics-pg.id}"
  key_name        = "${aws_key_pair.aws_deploy.id}"
  vpc_security_group_ids = ["${aws_security_group.cluster.id}"]
  subnet_id       = "${var.public_subnet["id"]}"
  private_ip      = "${lookup(var.instance_ips, count.index)}"
  associate_public_ip_address = true
}

resource "null_resource" "analytics-cluster" {
  count = 5
  # Changes to any instance of the cluster requires re-provisioning
  triggers {
    cluster_instance_ids = "${join(",", aws_instance.analytics-cluster.*.id)}"
  }
  provisioner "local-exec" {
    command = "echo -e ${element(aws_instance.analytics-cluster.*.public_ip, count.index)} \t ${lookup(var.instance_hostnames, count.index)} >> /etc/hosts;"
  }
}
